#!/usr/bin/python3

import logging
import gzip

from rdflib.graph import Graph
from rdflib.util import guess_format

from data.utils import is_gzip, is_readable


class KnowledgeBase:
    """ Knowledge Base Class for RDF
    A wrapper around an imported rdflib.Graph object with convenience functions
    """
    graph = None
    _property_distribution = {}

    def __init__(self, graph=None):
        self.logger = logging.getLogger(__name__)
        self.logger.debug("Initiating Knowledge Base")

        if graph is not None:
            if type(graph) is Graph:
                self.graph = graph
            elif type(graph) is str:
                self.graph = self._read(graph)
            else:
                raise TypeError(":: Wrong input type: {}; requires path to RDF"
                                " graph or rdflib.graph.Graph object".format(type(graph)))
        else:
            self.graph = Graph()

        self.logger.debug("Knowledge Base ({} facts) succesfully imported".format(len(self.graph)))

    def _read(self, path=None):
        assert is_readable(path)
        graph = Graph()

        if not is_gzip(path):
            graph.parse(path, format=guess_format(path))
        else:
            self.logger.debug("Input recognized as gzip file")
            with gzip.open(path, 'rb') as f:
                graph.parse(f, format=guess_format(path[:-3]))

        return graph

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.graph.destroy("store")
        self.graph.close(True)

    def __len__(self):
        return len(self.graph)

    ### Generators ###

    def atoms(self, pattern=(None, None, None)):
        self.logger.debug("Yielding atoms ({})".format(pattern))
        return self.graph.triples(pattern)

    def predicates(self):
        self.logger.debug("Yielding predicates")
        return self.graph.predicates()

    ## Operators
    def sample(self, strategy=None, **kwargs):
        """ Sample this graph using the given strategy
        returns a KnowledgeBase instance
        """
        if strategy is None:
            raise ValueError('Strategy cannot be left undefined')

        self.logger.debug("Sampling graph")
        return strategy.sample(self, **kwargs)
