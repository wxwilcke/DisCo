#!/usr/bin/python3

import logging

from rules.rule import Rule


class RuleBase:
    """ RuleBase class

    param rules: list of AMIE rules as [Rule, Rule, ...>]
    param metrics: list of AMIE rule metrics as [Metric, Metric, ...>]
    param label: label of AMIE rule metrics
    """
    rules = None
    label = None
    metrics = None

    def __init__(self, rules=[], metrics=[], label=''):
        self.logger = logging.getLogger(__name__)
        self.logger.debug("Initiating RuleBase")

        self.rules = rules
        self.label = label
        self.metrics = metrics

    def add(self, rule, metric=None):
        if type(rule) is not Rule:
            raise TypeError("Received {}, expected {}".format(type(rule),
                                                              Rule))

        self.rules.append(rule)
        self.metrics.append(metric)

    def rmv(self, rule):
        if type(rule) is not Rule:
            raise TypeError("Received {}, expected {}".format(type(rule),
                                                              Rule))

        if rule in self.rules:
            idx = self.rules.index(rule)
            del self.rules[idx]
            del self.metrics[idx]

    def sorted(self, by_metric=None, reverse=True):
        indices_sorted = sorted(range(len(self.metrics)),
                                key=lambda k: self.metrics[k][by_metric],
                                reverse=reverse)
        return RuleBase(rules=[self.rules[i] for i in indices_sorted],
                        metrics=[self.metrics[i] for i in indices_sorted],
                        label=self.label)

    def sort(self, by_metric=None, reverse=True):
        indices_sorted = sorted(range(len(self.metrics)),
                                key=lambda k: self.metrics[k].get(by_metric),
                                reverse=reverse)

        self.metrics = [self.metrics[i] for i in indices_sorted]
        self.rules = [self.rules[i] for i in indices_sorted]

    def __eq__(self, other):
        return self.label == other.label and\
                sorted(self.rules) == sorted(other.rules) and\
                sorted(self.metrics) == sorted(other.metrics)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __len__(self):
        return len(self.rules)
