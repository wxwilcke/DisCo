#!/usr/bin/python3

import logging
import re


class Rule:
    """ Horn rule class

    param head: an uninstantiated atom string a := '?lhs <predicate> ?rhs'
    param body: a list of uninstantiated atom strings [a_0, a_1, ..., a_N]
    """
    _ATOM_REGEX = "\?(?P<lhs>\w)\s+<(?P<predicate>[^>]+)>\s+\?(?P<rhs>\w)"

    head = None
    body = None

    def __init__(self, head, body=[]):
        self.head = self._decode_atom(head)
        self.body = [self._decode_atom(atom) for atom in body]

    def _decode_atom(self, atom):
        match = re.match(self._ATOM_REGEX, atom)
        if match is None:
            raise RuntimeError("Regex failure on atom: {}".format(atom))

        return Atom((match.group('lhs'),
                     match.group('predicate'),
                     match.group('rhs')))

    def __len__(self):
        return len(self.body) + 1

    def __eq__(self, other):
        return isinstance(other, self.__class__)\
                and self.head == other.head\
                and self.body == other.body

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__class__.__name__
                    + str(self.head)
                    + "".join([str(atom) for atom in self.body]))

    def __str__(self):
        return "{} => {}".format(", ".join(map(str, self.body)), self.head)


class Atom:
    """ Context aware wrapper around uninstantiated atom

    param fact: an uninstantiated atom P(lhs, rhs)
    """
    lhs = None
    rhs = None
    predicate = None

    def __init__(self, fact=(None, None, None)):
        self.logger = logging.getLogger(__name__)

        assert len(fact) == 3
        self.lhs = Variable(fact[0])
        self.predicate = fact[1]
        self.rhs = Variable(fact[2])

    def __eq__(self, other):
        return isinstance(other, self.__class__)\
                and self.predicate == other.predicate\
                and self.lhs == other.lhs\
                and self.rhs == other.rhs

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__class__.__name__
                    + str(self.predicate),
                    + str(self.lhs),
                    + str(self.rhs))

    def __str__(self):
        return "({}, {}, {})".format(self.lhs,
                                     self.predicate,
                                     self.rhs)

class Variable:
    """ Atom variable

    param name: a string
    """
    name = None

    def __init__(self, name=""):
        self.logger = logging.getLogger(__name__)

        self.name = name

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.name == other.name

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__class__.__name__ + self.name)

    def __str__(self):
        return self.name

class Metric:
    """ Rule metrics
    """
    _metrics = None

    def __init__(self, **kwargs):
        self.logger = logging.getLogger(__name__)

        self._metrics = dict()
        for key, value in kwargs.items():
            self._metrics[key] = value

    def get(self, metric):
        if metric in self._metrics.keys():
            return self._metrics[metric]

    def keys(self):
        return tuple(self._metrics.keys())

    def __contains__(self, key):
        return key in self._metrics

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__hash__() == other.__hash__()

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__class__.__name__ + self.__str__())

    def __len__(self):
        return len(self._metrics)

    def __str__(self):
        keys = list(self._metrics.keys())
        keys.sort()

        return "; ".join(["{}: {}".format(key, self._metrics[key])
                          for key in keys])
