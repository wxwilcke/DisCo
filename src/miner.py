#!/usr/bin/python3

import logging

from data.io.knowledge_base import KnowledgeBase
from data.utils import string_buffer
from interfaces.amie import AMIE, atom2tsv
from rules.rule_base import RuleBase


def run(graph):
    facts = set()
    with KnowledgeBase(graph) as kg:
        for lhs, predicate, rhs in kg.atoms():
            facts.add(atom2tsv(lhs.toPython(),
                               predicate.toPython(),
                               rhs.toPython()))

    buff = string_buffer(facts)

    rb = RuleBase()
    with AMIE(buff) as amie:
        rb.label = amie._AMIE_LABEL
        for rule, metric in amie.mine():
            rb.add(rule, metric)
