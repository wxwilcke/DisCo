#!/usr/bin/python3

import logging
from io import StringIO
import re
import os
from subprocess import check_output
import threading

from data.utils import buffer_pump
from rules.rule import Metric, Rule, Variable

class AMIE:
    """ Interface to AMIE
    """
    _JAVA_CMD = "/usr/bin/java"
    _JAVA_OPT = "-jar"
    _JAVA_ARG = "../../AMIE/amie_plus.jar"
    _AMIE_LABEL = ["Rule", "Head Coverage", "Std Confidence", "PCA Confidence",
                   "Positive Examples", "Body size", "PCA Body size",
                   "Functional variable", "Std. Lower Bound", "PCA Lower Bound",
                   "PCA Conf estimation"]
    _ATOM_REGEX = "\?\w\s+<[^>]+>\s+\?\w"

    _source_file = None
    _rule_base = None

    def __init__(self, knowledge_base):
        self.logger = logging.getLogger(__name__)
        self.logger.debug("Initiating AMIE")

        self._source_file = knowledge_base

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def mine(self):
        self.logger.debug("Starting mining process")
        output = ""
        if type(self._source_file) is StringIO:
            output = self._run_from_buffer()
        else:
            output = self._run_from_file()

        for line in output.split(b"\n"):
            line = line.decode('utf8')
            if self._is_ruleline(line):
                yield self._generate_rule(line)

    def _run_from_file(self):
        return check_output([self._JAVA_CMD,
                             self._JAVA_OPT,
                             self._JAVA_ARG,
                             self._source_file]).decode()

    def _run_from_buffer(self):
        output = ""
        r, w = os.pipe()
        try:
            threading.Thread(target=buffer_pump,
                             args=[open(w, 'w'), self._source_file]).start()
            output = check_output([self._JAVA_CMD,
                                   self._JAVA_OPT,
                                   self._JAVA_ARG,
                                   '/dev/fd/'+str(r)],
                                   pass_fds=[r])
        finally:
            os.close(r)

        return output

    def _generate_rule(self, ruleline):
        splitted_ruleline = ruleline.split("\t")
        return (self._decode_rule(splitted_ruleline[0]),
                self._decode_metrics(splitted_ruleline[1:]))

    def _decode_rule(self, rule_string):
        splitted_rule_string = re.findall(self._ATOM_REGEX, rule_string)
        if len(splitted_rule_string) <= 1:
            raise RuntimeError("Regex failure on rule string: {}".format(rule_string))

        return Rule(head=splitted_rule_string[-1],
                    body=splitted_rule_string[:-1])

    def _decode_metrics(self, metrics):
        return Metric(head_coverage=float(metrics[0]),
                      standard_confidence=float(metrics[1]),
                      pca_confidence=float(metrics[2]),
                      positive_examples=int(metrics[3]),
                      body_size=int(metrics[4]),
                      pca_body_size=int(metrics[5]),
                      functional_variable=Variable(metrics[6].lstrip("?")),
                      standard_lower_bound=float(metrics[7]),
                      pca_lower_bound=float(metrics[8]),
                      pca_confidence_estimation=float(metrics[9]))

    def _is_ruleline(self, line):
        if re.match("(?:{}\s+)+=>\s+{}".format(self._ATOM_REGEX,
                                               self._ATOM_REGEX),
                    line) is not None:
            return True

        return False

def atom2tsv(lhs, predicate, rhs):
    return "<{}>\t<{}>\t<{}>.".format(lhs,
                                      predicate,
                                      rhs)
